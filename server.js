const express = require('express');

class App {
    constructor() {
        this.base = '/api/v1';
        this.port = process.env.PORT || 5000;
        this.app = express();
    }

    initMiddlewares() {
        const cors = require('cors');
        const jwt = require('./helpers/jwt');
        const bodyParser = require('body-parser');
        this.app.use(bodyParser.urlencoded({ extended: true }));
        this.app.use(bodyParser.json());
        this.app.use(jwt());
        this.app.use(cors());
    }

    initSwagger() {
        const swaggerUi = require('swagger-ui-express');
        const doc = require('./swagger.json');
        this.app.use(`${this.base}/api-docs`, swaggerUi.serve, swaggerUi.setup(doc));
    }


    initRoutes() {
        this.app.use(`${this.base}/entries`, require('./auth/auth.controller'));
        this.app.use(`${this.base}/profile`, require('./profile/profile.controller'));
        this.app.use(`${this.base}/posts`, require('./post/post.controller'));
    }

    runServer() {
        const { port } = this;
        this.app.listen(port, function() {
            console.log('Server listening on port ' + port);
        });
    }

    init() {
        this.initMiddlewares();
        this.initRoutes();
        this.app.use(require('./helpers/error-handler'));
        this.initSwagger();
        this.runServer();

    }
}

module.exports = new App().base;

new App().init();
