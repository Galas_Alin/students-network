const expressJwt = require('express-jwt');
const config = require('../config.json');
const profileService = require('../profile/profile.service');
const base = require('../server');

module.exports = jwt;

function jwt() {
    const secret = config.secret;
    return expressJwt({ secret }).unless({
        path: [
            // public routes that don't require authentication
            new RegExp(base + '/api-docs'),
            `${base}/entries/register`,
            `${base}/entries/login`,
            `${base}/entries/forgot_password`,
            `${base}/entries/reset_password`
        ]
    });
}

async function isRevoked(req, payload, done) {
    const user = await profileService.getById(payload.sub);

    // revoke token if auth no longer exists
    if (!user) {
        return done(null, true);
    }

    done(user, true);
}
