const RESPONSE_TYPE = {
    SUCCESS: {
        ok: true
    },
    FAIL: {
        ok: false
    }
};

const ERROR_TYPES = {
    VALIDATION_ERROR: 'ValidationError',
    UNAUTHORIZE_ERROR: 'UnauthorizedError',
    NOT_FOUND: 'NotFound'
};

module.exports = {
    RESPONSE_TYPE,
    ERROR_TYPES
};
