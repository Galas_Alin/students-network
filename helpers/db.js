const config = require('../config.json');
const mongoose = require('mongoose');

mongoose.connect(config.connectionStringLab,  
    { 
        useNewUrlParser: true, 
        useCreateIndex: true, 
    }
);
mongoose.Promise = global.Promise;

module.exports = {
    User: require('../models/user.model'),
    Post: require('../models/post.model')
};