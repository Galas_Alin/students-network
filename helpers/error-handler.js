const { ERROR_TYPES } = require('./constants');

module.exports = errorHandler;



function errorHandler(err, req, res, next) {
    if (typeof (err) === 'string') {
        // custom application error
        return res.status(400).json({ error: true, message: err });
    }

    if (err.name === ERROR_TYPES.VALIDATION_ERROR) {
        // mongoose validation error
        return res.status(400).json({ error: true, message: err.message });
    }

    if (err.name === ERROR_TYPES.NOT_FOUND) {
        return res.status(422).json({ error: true, message: err.message });
    }

    if (err.name === ERROR_TYPES.UNAUTHORIZE_ERROR) {
        // jwt authentication error
        return res.status(401).json({ error: true, message: err.message });
    }

    // default to 500 server error
    return res.status(500).json({ error: true, message: err.message });
}