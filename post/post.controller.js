const PostService = require('./post.service');
const express = require('express');
const multer = require("multer");
const cloudinaryStorage = require("multer-storage-cloudinary");
const { RESPONSE_TYPE } =  require('../helpers/constants');
const cloudinary = require('cloudinary');
const config = require('../config.json');


class PostController {
    constructor() {
        this.router = express.Router();
        this.postService = PostService;
        this.configureCloudinary();
        this.createStorage();
        this.parser = multer({ storage: this.storage });
        this.routerSetup();
    }

    async getNews(req, res, next) {
        let method;
        if (req.query.userId) {
            method = this.postService.getNewsByUserId;
        } else {
            method = this.postService.getNews;
        }
        method.call(this.postService, req, res, next)
            .then(posts => res.json({...RESPONSE_TYPE.SUCCESS, data: {posts}}))
            .catch(err => next(err));
    }

    async createPost(req, res, next) {
        this.postService.createPost(req, res, next)
            .then(post => res.json({...RESPONSE_TYPE.SUCCESS, data: {post}}))
            .catch(err => next(err));
    }

    async removePost(req, res, next) {
        this.postService.removePost(req, res, next)
            .then(post => res.json({...RESPONSE_TYPE.SUCCESS, data: post}))
            .catch(err => next(err));
    }

    async commentPost(req, res, next) {
        this.postService.commentPost(req, res, next)
            .then(post => res.json({...RESPONSE_TYPE.SUCCESS, data: {post}}))
            .catch(err => next(err));
    }

    async removeComment(req, res, next) {
        this.postService.removeComment(req, res, next)
            .then(post => res.json({...RESPONSE_TYPE.SUCCESS, data: {post}}))
            .catch(err => next(err));
    }

    async likeDislikePost(req, res, next, process) {
        this.postService.likeDislikePost(req, process)
            .then(post => res.json({...RESPONSE_TYPE.SUCCESS, data: {post}}))
            .catch(err => next(err));
    }

    routerSetup() {
        this.router.get('/', (req, res, next) => this.getNews(req, res, next));
        this.router.post('/', this.parser.single('image'), (req, res, next) => this.createPost(req, res, next));
        this.router.delete('/:id', (req, res, next) => this.removePost(req, res, next));
        this.router.put('/:id/like', (req, res, next) => this.likeDislikePost(req, res, next, 'like'));
        this.router.put('/:id/dislike', (req, res, next) => this.likeDislikePost(req, res, next, 'dislike'));
        this.router.post('/:id/comment', (req, res, next) => this.commentPost(req, res, next));
        this.router.delete('/:id/comment/:commentId', (req, res, next) => this.removeComment(req, res, next));
    }

    configureCloudinary() {
        const { cloudinaryConfig } = config;
        cloudinary.config({
            cloud_name: cloudinaryConfig.cloud_name,
            api_key: cloudinaryConfig.api_key,
            api_secret: cloudinaryConfig.secret_key
        });
    }

    createStorage() {
        this.storage = cloudinaryStorage({
            cloudinary,
            folder: "posts",
            allowedFormats: ["jpg", "png"],
            transformation: [{ width: 500, height: 500, crop: "limit" }]
        });
    }
}

module.exports = new PostController().router;