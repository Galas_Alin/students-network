const {ERROR_TYPES} = require('../helpers/constants');

class PostService {
    constructor() {
        this.db = require('../helpers/db');
        this.User = this.db.User;
        this.Post = this.db.Post;

    }

    async getNews(req) {
        const _id = req.params.id || req.user.sub;
        if (!_id) {
            const error = new Error('User has no found');
            error.name = ERROR_TYPES.UNAUTHORIZE_ERROR;
            throw error;
        }
        const posts = await this.Post.find({});
        return posts.sort((a, b) => b.date - a.date);

    };

    async createPost(req) {
        const parsedBody = JSON.parse(req.body.data);
        const publishUserId = parsedBody.publishUserId || req.user.sub;
        const {text, header} = parsedBody;
        const creatorId = req.user.sub;
        const imageUrl = (req.file || {}).url;
        if (!creatorId) {
            const error = new Error('User has no found');
            error.name = ERROR_TYPES.UNAUTHORIZE_ERROR;
            throw error;
        }
        const user = await this.getById(creatorId);
        if (!user) {
            const error = new Error('User has no found or removed from DB');
            error.name = ERROR_TYPES.UNAUTHORIZE_ERROR;
            throw error;
        }
        const {name, surname, id, avatarUrl, fullname} = user;
        const post = new this.Post({
            author: {
                name,
                surname,
                id,
                avatarUrl,
                fullname
            },
            creatorId,
            publishUserId,
            avatarUrl,
            text,
            header,
            imageUrl
        });
        await post.save();
        return post.toObject();
    }


    async getNewsByUserId(req) {
        const _id = req.query.userId;
        if (!_id) {
            const error = new Error('User has no found');
            error.name = ERROR_TYPES.UNAUTHORIZE_ERROR;
            throw error;
        }
        const posts = await this.Post.find({creatorId: _id});
        return posts.sort((a, b) => b.date - a.date);

    };

    async removePost(req) {
        const { id } = req.params;
        const post = await this.Post.findByIdAndRemove({_id: id});
        if (!post) {
            const error = new Error('The Post has no found');
            error.name = ERROR_TYPES.VALIDATION_ERROR;
            throw error;
        }
        return {};
    }

    async likeDislikePost(req, process) {
        const _id = req.params.id;
        const creatorId = req.user.sub;
        const post = await this.getPostById({_id});
        if (!post) {
            const error = new Error('The Post has no found');
            error.name = ERROR_TYPES.VALIDATION_ERROR;
            throw error;
        }
        return await this.behaveLikesPost(post, creatorId, process);
    }

    async behaveLikesPost(post, _id, process) {
        const likeIndex = post.likes.indexOf(_id);
        const dislikeIndex = post.dislikes.indexOf(_id);
        switch(process) {
            case 'like': {
                dislikeIndex > -1 ? post.dislikes.splice(dislikeIndex, 1) : '';
                likeIndex > -1 ? post.likes.splice(likeIndex, 1) : post.likes.push(_id);
                break;
            }
            case 'dislike': {
                dislikeIndex > -1 ? post.dislikes.splice(dislikeIndex, 1) : post.dislikes.push(_id);
                likeIndex > -1 ? post.likes.splice(likeIndex, 1) : '';
                break;
            }
        }
        return await post.save();
    }



    async dislikePost(req) {}

    async commentPost(req) {
        if (!req.body.text) {
            const error = new Error('No text found');
            error.name = ERROR_TYPES.VALIDATION_ERROR;
            throw error;
        }
        const _id = req.params.id;
        const userId = req.user.sub;
        const user = await this.getById(userId);
        if (!user) {
            const error = new Error('User has no found');
            error.name = ERROR_TYPES.UNAUTHORIZE_ERROR;
            throw error;
        }
        const {name, surname, id, avatarUrl, fullname} = user;

        const post = await this.Post.findOneAndUpdate(
            {_id},
            {
                $push: {
                    comments: {
                        $each: [
                            {
                                author: {
                                    name, surname, id, avatarUrl, fullname
                                },
                                text: req.body.text
                            }
                        ],
                        $position: 0
                    },
                }
            }, {new: true}
        );
        if (!post) {
            const error = new Error('The Post has no found');
            error.name = ERROR_TYPES.VALIDATION_ERROR;
            throw error;
        }
        return post;
    }

    async removeComment(req) {
        const { id, commentId } = req.params;
        const post = await this.getPostById({_id: id});
        if (!post) {
            const error = new Error('The Post has no found');
            error.name = ERROR_TYPES.VALIDATION_ERROR;
            throw error;
        }
        const comment = await post.comments.id(commentId);
        if (!comment) {
            const error = new Error('The Comment has no found');
            error.name = ERROR_TYPES.VALIDATION_ERROR;
            throw error;
        }
        await comment.remove();
        return await post.save();
    }


    async getById(_id) {
        return await this.User.findOne({_id});
    }

    async getPostById(_id) {
        return await this.Post.findOne({_id});
    }
}

module.exports = new PostService();