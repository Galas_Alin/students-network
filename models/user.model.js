const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const contactScheme = new Schema({
    country: 'string',
    city: 'string',
    street: 'string',
    mobilePhone: 'number',
    skype: 'string'
});

const educationScheme = new Schema({
    country: 'string',
    city: 'string',
    school: 'number',
    university: 'string',
    periodOfSchool: 'number',
    periodOfUniversity: 'number'
});

const workScheme = new Schema({
    country: 'string',
    city: 'string',
    company: 'string',
    period: 'number',
});


const schema = new Schema({
    avatarUrl: 'string',
    name: { type: 'string', required: true },
    surname: { type: 'string', required: true },
    fullname: { type: 'string', required: true},
    email: {
        type: String,
        required: true,
        validate: {
            validator: v => /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test(v),
            message: props => `${props.value} is not a valid email!`
        }
    },
    subscribers: ['string'],
    subscriptions: ['string'],
    interests: ['string'],
    contact: contactScheme,
    education: educationScheme,
    work: [workScheme],
    gender: { type: 'string', required: false },
    birthDate: {type: Date, required: false },
    hash: { type: String, required: true },
    resetPasswordToken: {
        type: 'string'
    },
    resetPasswordExpires: {
        type: Date
    }
});


schema.set('toJSON', { virtuals: true });
schema.set('toObject', { virtuals: true });

module.exports = mongoose.model('User', schema);