const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userCard = {
    name: 'string',
    surname: 'string',
    fullname: 'string',
    id: 'string',
    avatarUrl: 'string'
};

const comment = new Schema({
    author: userCard,
    text: {type: 'string', required: true},
    likes: ['string'],
    dislikes: ['string'],
    date: { type: Date, default: Date.now()},
});

const schema = new Schema({
    author: userCard,
    comments: [comment],
    creatorId: {type: 'string', required: true},
    dislikes: ['string'],
    date: { type: Date, default: Date.now()},
    likes: ['string'],
    header: { type: 'string', default: '' },
    imageUrl: { type: 'string', default: '' },
    publishUserId: 'string',
    text: { type: 'string', default: '' },
});

schema.set('toJSON', { virtuals: true });
schema.set('toObject', { virtuals: true });

module.exports = mongoose.model('Post', schema);