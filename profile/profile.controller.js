const ProfileService = require('./profile.service');
const express = require('express');
const multer = require("multer");
const cloudinaryStorage = require("multer-storage-cloudinary");
const { RESPONSE_TYPE } =  require('../helpers/constants');
const cloudinary = require('cloudinary');
const config = require('../config.json');


class ProfileController {
    constructor() {
        this.router = express.Router();
        this.profileService = ProfileService;
        this.configureCloudinary();
        this.createStorage();
        this.parser = multer({ storage: this.storage });
        this.routerSetup();
    }

    async getUserProfile(req, res, next) {
        this.profileService.getProfileData(req)
            .then(user => res.json({...RESPONSE_TYPE.SUCCESS, data: user}))
            .catch(err => next(err));
    }

    async updateProfile(req, res, next) {
        this.profileService.updateProfile(req)
            .then(user => res.json({...RESPONSE_TYPE.SUCCESS, data: user}))
            .catch(err => next(err));
    }

    async uploadAvatar(req, res, next) {
        this.profileService.uploadUserAvatar(req)
            .then(avatarUrl => res.json({...RESPONSE_TYPE.SUCCESS, data: {avatarUrl}}))
            .catch(err => next(err));
    }

    async searchProfile(req, res, next) {
        this.profileService.searchProfile(req)
            .then(users => res.json({...RESPONSE_TYPE.SUCCESS, data: users}))
            .catch(err => next(err));
    }

    async subscribe(req, res, next) {
        this.profileService.subscribe(req)
            .then(subscriptions => res.json({...RESPONSE_TYPE.SUCCESS, data: {subscriptions}}))
            .catch(err => next(err));
    }

    async unsubscribe(req, res, next) {
        this.profileService.unsubscribe(req)
            .then(subscriptions => res.json({...RESPONSE_TYPE.SUCCESS, data: {subscriptions}}))
            .catch(err => next(err));
    }

    async getSubscriptions(req, res, next) {
        this.profileService.getSubscriptions(req)
            .then(subscriptions => res.json({...RESPONSE_TYPE.SUCCESS, data: {subscriptions}}))
            .catch(err => next(err));
    }

    async getSubscribers(req, res, next) {
        this.profileService.getSubscribers(req)
            .then(subscribers => res.json({...RESPONSE_TYPE.SUCCESS, data: {subscribers}}))
            .catch(err => next(err));
    }


    routerSetup() {

        this.router.get('/', (req, res, next) => this.getUserProfile(req, res, next));
        this.router.get('/search', (req, res, next) => this.searchProfile(req, res, next));
        this.router.get('/subscriptions', (req, res, next) => this.getSubscriptions(req, res, next));
        this.router.get('/subscribers', (req, res, next) => this.getSubscribers(req, res, next));
        this.router.get('/:id', (req, res, next) => this.getUserProfile(req, res, next));
        this.router.get('/:id/subscribers', (req, res, next) => this.getSubscribers(req, res, next));
        this.router.get('/:id/subscriptions', (req, res, next) => this.getSubscriptions(req, res, next));
        this.router.post('/:id/subscribe', (req, res, next) => this.subscribe(req, res, next));
        this.router.post('/:id/unsubscribe', (req, res, next) => this.unsubscribe(req, res, next));
        this.router.put('/', (req, res, next) => this.updateProfile(req, res, next));
        this.router.post('/upload_avatar', this.parser.single('image'), (req, res, next) => this.uploadAvatar(req, res, next));
    }

    configureCloudinary() {
        const { cloudinaryConfig } = config;
        cloudinary.config({
            cloud_name: cloudinaryConfig.cloud_name,
            api_key: cloudinaryConfig.api_key,
            api_secret: cloudinaryConfig.secret_key
        });
    }

    createStorage() {
        this.storage = cloudinaryStorage({
            cloudinary,
            folder: "avatar",
            allowedFormats: ["jpg", "png"],
            transformation: [{ width: 500, height: 500, crop: "limit" }]
        });
    }
}

module.exports = new ProfileController().router;