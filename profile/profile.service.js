const { ERROR_TYPES } = require('../helpers/constants');

class ProfileService {
    constructor() {
        this.db = require('../helpers/db');
        this.User = this.db.User;

    }

    async getProfileData(req) {
        const _id = req.params.id || req.user.sub;
        if (!_id) {
            const error = new Error('User has no found');
            error.name = ERROR_TYPES.UNAUTHORIZE_ERROR;
            throw error;
        }
        const user = await this.User.findOne({_id});
        if (!user) {
            const error = new Error('User has no found');
            error.name = ERROR_TYPES.UNAUTHORIZE_ERROR;
            throw error;
        }
        const { hash, ...userWithoutHash } = user.toObject();
        return {
            user: userWithoutHash
        }
    };

    async updateProfile(req) {
        const _id = req.user.sub;
        if (!_id) {
            const error = new Error('User has no found');
            error.name = ERROR_TYPES.UNAUTHORIZE_ERROR;
            throw error;
        }
        const user = await this.User.findOneAndUpdate({_id}, {...req.body}, {new: true});
        if (!user) {
            const error = new Error('User has no found');
            error.name = ERROR_TYPES.UNAUTHORIZE_ERROR;
            throw error;
        }
        const { hash, ...userWithoutHash } = user.toObject();
        return {
            user: userWithoutHash
        }
    };

    async uploadUserAvatar(req) {
        const _id = req.user.sub;
        if (!_id) {
            const error = new Error('User has no found');
            error.name = ERROR_TYPES.UNAUTHORIZE_ERROR;
            throw error;
        }
        const { url } = req.file;
        const user = await this.User.findOneAndUpdate({_id}, {avatarUrl: url});
        if (!user) {
            const error = new Error('User has no found');
            error.name = ERROR_TYPES.UNAUTHORIZE_ERROR;
            throw error;
        }
        return url;
    };

    async subscribe(req) {
        const _id = req.user.sub;
        if (!_id) {
            const error = new Error('User has no found');
            error.name = ERROR_TYPES.UNAUTHORIZE_ERROR;
            throw error;
        }
        const subId = req.params.id;
        const subUser = await this.User.findOne({_id: subId});
        const user = await this.User.findOne({_id});

        if (!user) {
            const error = new Error('User with '+ req.params.id +' has no found');
            error.name = ERROR_TYPES.UNAUTHORIZE_ERROR;
            throw error;
        }

        if (subUser.subscribers.includes(_id)) {
            const error = new Error('The User has already subscribed on user with id='+ id +'.');
            error.name = ERROR_TYPES.VALIDATION_ERROR;
            throw error;
        }
        subUser.subscribers.push(_id);
        user.subscriptions.push(subId);

        await subUser.save();
        await user.save();
        return user.subscriptions;
    };

    async unsubscribe(req) {
        const _id = req.user.sub;
        if (!_id) {
            const error = new Error('User has no found');
            error.name = ERROR_TYPES.UNAUTHORIZE_ERROR;
            throw error;
        }
        const subId = req.params.id;
        const subUser = await this.User.findOne({_id: subId});
        const user = await this.User.findOne({_id});

        if (!user) {
            const error = new Error('User with '+ req.params.id +' has no found');
            error.name = ERROR_TYPES.UNAUTHORIZE_ERROR;
            throw error;
        }

        if (!user.subscriptions.includes(subId)) {
            const error = new Error('The User has\'t subscribed on user with id='+ id +' yet.');
            error.name = ERROR_TYPES.VALIDATION_ERROR;
            throw error;
        }
        const userIndex = user.subscriptions.indexOf(subId);
        const subIndex = subUser.subscribers.indexOf(_id);
        subUser.subscribers.splice(subIndex, 1);
        user.subscriptions.splice(userIndex, 1);
        await subUser.save();
        await user.save();
        return user.subscriptions;
    };

    async getSubscriptions(req) {
        const _id = req.params.id || req.user.sub;
        if (!_id) {
            const error = new Error('User has no found');
            error.name = ERROR_TYPES.UNAUTHORIZE_ERROR;
            throw error;
        }
        const user = await this.User.findOne({_id});
        if (!user) {
            const error = new Error('User has no found');
            error.name = ERROR_TYPES.UNAUTHORIZE_ERROR;
            throw error;
        }
        const { subscriptions } = user.toObject();

        const subscriptionsPromises = await subscriptions.map(_id =>
            (async (_id) => {
                const user = await this.getById(_id);
                if (!user) {
                    return;
                }
                const {name, surname, fullname, id, avatarUrl} = user.toObject();
                return {name, surname, fullname, id, avatarUrl}
            })(_id)
        );
        return Promise.all(subscriptionsPromises);
    };

    async getSubscribers(req) {
        const _id = req.params.id || req.user.sub;
        if (!_id) {
            const error = new Error('User has no found');
            error.name = ERROR_TYPES.UNAUTHORIZE_ERROR;
            throw error;
        }
        const user = await this.User.findOne({_id});
        if (!user) {
            const error = new Error('User has no found');
            error.name = ERROR_TYPES.UNAUTHORIZE_ERROR;
            throw error;
        }
        const { subscribers } = user.toObject();

        const subscribersPromises = await subscribers.map(_id =>
            (async (_id) => {
                const user = await this.getById(_id);
                if (!user) {
                    return;
                }
                const {name, surname, fullname, id, avatarUrl} = user.toObject();
                return {name, surname, fullname, id, avatarUrl}
            })(_id)
        );
        return Promise.all(subscribersPromises);
    };

    async searchProfile(req) {
        const query = req.query.fullname;
        if (!query) {
            const error = new Error('User has no found');
            error.name = ERROR_TYPES.UNAUTHORIZE_ERROR;
            throw error;
        }
        const users = await this.User
            .find({fullname: new RegExp(query, 'i')})
            .select('name surname fullname id avatarUrl')
            .lean()
            .limit(20);
        return {users};
    };

    async getById(_id) {
        return await this.User.findOne({_id });
    }
}

module.exports = new ProfileService();