const AuthService = require('./auth.service');
const express = require('express');
const { RESPONSE_TYPE } =  require('../helpers/constants');
const path = require('path');
const hbs = require('nodemailer-express-handlebars');
const nodemailer = require('nodemailer');


class AuthController {
    constructor() {
        this.smtpTransport = null;
        this.router = express.Router();
        this.authService = AuthService;
        this.email = process.env.MAILER_EMAIL_ID || 'socialnetworka13@gmail.com';
        this.pass = process.env.MAILER_PASSWORD || 'qwerty158P';
        this.createTransport();
        this.routerSetup();
        this.handlebarsOptions = {
            viewEngine: 'handlebars',
            viewPath: path.resolve('./templates/'),
            extName: '.html'
        };
        this.smtpTransport.use('compile', hbs(this.handlebarsOptions));
    }

    register(req, res, next) {
        this.authService.register(req.body)
            .then(t => res.json({...RESPONSE_TYPE.SUCCESS, data: t}))
            .catch(err => next(err));
    }

    login(req, res, next) {
        this.authService.authenticate(req.body)
            .then(t => t ? res.json({...RESPONSE_TYPE.SUCCESS, data: t}) : res.status(400).json({...RESPONSE_TYPE.FAIL, message: 'Code or password is incorrect' }))
            .catch(err => next(err));
    }

    forgotPassword(req, res, next) {
        this.authService.forgotPassword(req.body)
            .then(data => this.sendForgotPasswordEmail(data))
            .then(() => res.json({...RESPONSE_TYPE.SUCCESS, data: {}}))
            .catch(err => next(err));
    }

    resetPassword(req, res, next) {
        this.authService.resetPassword(req.body)
            .then(data => this.sendResetPasswordEmail(data))
            .then(() => res.json({...RESPONSE_TYPE.SUCCESS, data: {}}))
            .catch(err => next(err));
    }

    async sendForgotPasswordEmail({name, token, email}) {
        const mailOptions = {
            to: email,
            from: this.email,
            template: 'forgot-password-email',
            subject: 'Reset Password',
            context: {
                url: 'http://localhost:4200/auth/reset_password?token=' + token,
                name
            }
        };
        try {
            return await this.smtpTransport.sendMail(mailOptions);
        } catch(error) {
            throw new Error(error);
        }
    }

    async sendResetPasswordEmail({name, email}) {
        const mailOptions = {
            to: email,
            from: this.email,
            template: 'reset-password-email',
            subject: 'Password Reset Confirmation',
            context: {
                name
            }
        };
        try {
            return await this.smtpTransport.sendMail(mailOptions);
        } catch(error) {
            throw new Error(error);
        }
    }

    routerSetup() {
        this.router.post('/register', (req, res, next) => this.register(req, res, next));
        this.router.post('/login', (req, res, next) => this.login(req, res, next));
        this.router.post('/forgot_password', (req, res, next) => this.forgotPassword(req, res, next));
        this.router.post('/reset_password', (req, res, next) => this.resetPassword(req, res, next));
    }

    createTransport() {
        this.smtpTransport = nodemailer.createTransport({
            service: process.env.MAILER_SERVICE_PROVIDER || 'Gmail',
            auth: {
                user: this.email,
                pass: this.pass
            }
        });
    }

}

module.exports = new AuthController().router;