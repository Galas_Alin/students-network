const bcrypt = require('bcryptjs');
const config = require('../config.json');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const { ERROR_TYPES } = require('../helpers/constants');

class AuthService {

    constructor() {
        const db = require('../helpers/db');
        this.User = db.User;
        this.expTime = 86400000;
    }

    async register(userParams) {

        if (await this.User.findOne({ email: userParams.email })) {
            const error = new Error(`Email ${userParams.email} has already taken`);
            error.name = ERROR_TYPES.VALIDATION_ERROR;
            throw error;
        }

        const user = new this.User({...userParams, fullname: `${userParams.name} ${userParams.surname}` });

        // hash password
        if (userParams.password) {
            user.hash = bcrypt.hashSync(userParams.password, 10);
        }
        await user.save();
        const token = jwt.sign({ sub: user.id }, config.secret);
        return {
            token
        }
    }

    async authenticate({ email, password }) {
        const user = await this.User.findOne({ email });
        if (user && bcrypt.compareSync(password, user.hash)) {
            const token = jwt.sign({ sub: user.id }, config.secret);
            return {
                token
            };
        } else {
            const error = new Error('Email or password is incorrect');
            error.name = ERROR_TYPES.UNAUTHORIZE_ERROR;
            throw error;
        }
    }

    async forgotPassword({email}) {
        const user = await this.User.findOne({ email });
        if (user) {
            const buffer = crypto.randomBytes(20);
            const token = buffer.toString('hex');
            const userUpd = await this.User.findOneAndUpdate
                (
                    {_id: user._id},
                    {resetPasswordToken: token, resetPasswordExpires: Date.now() + this.expTime}
                );
            const {name, email} = userUpd;
            return {
                name,
                email,
                token
            }

        } else {
            const error = new Error('auth has not found');
            error.name = ERROR_TYPES.NOT_FOUND;
            throw error;
        }
    }

    async resetPassword({token, password}) {
        const user = await this.User.findOne({resetPasswordToken: token, resetPasswordExpires: {$gt: Date.now()}});
        if (user) {
            user.hash = bcrypt.hashSync(password, 10);
            user.resetPasswordToken = undefined;
            user.resetPasswordExpires = undefined;
            await user.save();
            const {name, email} = user;
            return {
                name,
                email
            }

        } else {
            const error = new Error('Password reset token is invalid or has expired');
            error.name = ERROR_TYPES.NOT_FOUND;
            throw error;
        }
    };
}

module.exports = new AuthService();